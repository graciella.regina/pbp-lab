from django.urls import path
from .views import index
from .views import add_friend

urlpatterns = [
    path('', index, name='index'),
    path('add',add_friend, name ='add'),
    #path('friends/', friend_list, name='friends')
    # TODO Add friends path using friend_list Views
]
