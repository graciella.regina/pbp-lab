/// Flutter code sample for TabBar

// [TabBar] can also be implemented by using a [TabController] which provides more options
// to control the behavior of the [TabBar] and [TabBarView]. This can be used instead of
// a [DefaultTabController], demonstrated below.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  static String _title = 'iCovid Home';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
/// AnimationControllers can be created with `vsync: this` because of TickerProviderStateMixin.
class _MyStatefulWidgetState extends State<MyStatefulWidget>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('iCovid'),
        bottom: TabBar(
          controller: _tabController,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home_outlined),
            ),
            Tab(
              icon: Icon(Icons.comment),
            ),
            Tab(
              icon: Icon(Icons.person),
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    child: Text("iCovid",
                        style: TextStyle(fontWeight: FontWeight.bold))),
                Container(
                    color: Colors.lightBlue[100],
                    width: 200,
                    height: 50,
                    child: Text(
                      "iCovid is an app where people can exchange informations and discuss topics about Covid",
                      maxLines: 4,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    )),
              ]),
          Container(
              margin: EdgeInsets.only(top: 40.0, bottom: 20.0),
              child: Center(
                  child: Text("Look for latest news",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 14)))),
          Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    color: Colors.lightBlue[100],
                    width: 300,
                    height: 100,
                    child: Text(
                      "iCovid is an app where people can exchange informations and discuss topics about Covid",
                      maxLines: 4,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    )),
                Container(
                  margin: EdgeInsets.only(bottom: 20.0),
                  child: Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      onTap: () {
                        print('News.');
                      },
                      child: SizedBox(
                        width: 300,
                        height: 100,
                        child: Text('A card that can be tapped'),
                      ),
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    color: Colors.lightBlue[100],
                    width: 300,
                    height: 100,
                    child: Text(
                      "iCovid is an app where people can exchange informations and discuss topics about Covid",
                      maxLines: 4,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                    ))
              ]),
          Center(
            child: Text("It's rainy here"),
          ),
          Center(
            child: Text("It's sunny here"),
          ),
        ],
      ),
    );
  }
}
