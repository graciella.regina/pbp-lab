/// Flutter code sample for TabBar

// [TabBar] can also be implemented by using a [TabController] which provides more options
// to control the behavior of the [TabBar] and [TabBarView]. This can be used instead of
// a [DefaultTabController], demonstrated below.

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text("iCovid Home"),
            ),
            body: Container(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                            child: Text("iCovid",
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        Container(
                            color: Colors.grey[100],
                            width: 200,
                            height: 50,
                            child: Text(
                              "iCovid is an app where people can exchange informations and discuss topics about Covid",
                              maxLines: 4,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            )),
                      ]),
                  Container(
                      margin: const EdgeInsets.only(top: 40.0, bottom: 20.0),
                      child: Center(
                          child: Text("Look for latest news",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)))),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(bottom: 20.0),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue[100],
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          width: 320,
                          height: 120,
                          child: Row(children: [
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 150,
                                height: 120,
                                child: new Image.asset('assets/virus.jpg')),
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 100,
                                child: Text(
                                  'Virus Corona membuat warga semakin resah',
                                  maxLines: 3,
                                ))
                          ]),
                          //child: new Image.asset('assets/virus.jpg'),
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 20.0),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue[100],
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          width: 320,
                          height: 120,
                          child: Row(children: [
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 150,
                                height: 120,
                                child: new Image.asset('assets/jumlah.jpg')),
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 100,
                                child: Text(
                                  'Data paling update covid di Indonesia',
                                  maxLines: 3,
                                ))
                          ]),
                          //child: new Image.asset('assets/virus.jpg'),
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 20.0),
                          decoration: BoxDecoration(
                            color: Colors.lightBlue[100],
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          width: 320,
                          height: 120,
                          child: Row(children: [
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 150,
                                height: 120,
                                child: new Image.asset('assets/stop.jpg')),
                            Container(
                                margin: const EdgeInsets.only(left: 20.0),
                                width: 100,
                                child: Text(
                                  'Cara mencegah penyebaran virus corona',
                                  maxLines: 3,
                                ))
                          ]),
                        )
                      ])
                ]),
              ),
            )));
  }
}
