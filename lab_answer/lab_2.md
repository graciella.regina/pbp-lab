Kemudian juga ada beberapa pertanyaan singkat yang perlu dijawab dalam file lab_answer/lab_2.md, yaitu:

Apakah perbedaan antara JSON dan XML?
Apakah perbedaan antara HTML dan XML?

Dari definisinya, JSON adalah turunan JavaScript yang digunakan dalam transfer dan penyimpanan data. Kekinian, bahasa ini sering dimanfaatkan dalam pembuatan aplikasi web. Sedangkan XML merupakan bahasa markup yang diciptakan untuk menyederhanakan proses penyimpanan dan pengiriman data antarserver. 

Perbedaan antara JSON dan XML :
JSON:
-Menyimpan data dalam syntax yang mirip dengan JavaScript
-Mendukung UTF dan ASCII
-Data disimpan seperti map dengan pasangan key-value
-Mendukung angka, array, string, boolean, dah objek


XML :
-Memiliki ciri khas berupa tag dan syntax-nya mirip dengan HTML
-Mendukung pengkodean UTF-8 dan UTF-16
-Data disimpan sebagai tree structure
-Mendukung data type yang bervariasi, seperti gambar, teks, dan grafik


Perbedaan antara HTML dan XML : 
-HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web
-Extensible Markup Language (XML) digunakan untuk membantu untuk bertukar data antar platform yang berbeda.

Case Sensitivity :
-HTML tidak case sensitive
-XML case sensitive

Tag:
-HTML memiliki tag yang telah ditentukan sebelumnya dan penggunaan tag penutup opsional
-Programmer mendefinisikan kumpulan tagnya XML dan wajib menggunakan tag penutup

Penggunaan :
-HTML dirancang dengan penekanan pada fitur penyajian data
-XML adalah data spesifik di mana penyimpanan dan transfer data menjadi perhatian utama