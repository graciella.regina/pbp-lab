from django.shortcuts import render
from .forms import NoteForm
from lab_2.models import Note
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
          return HttpResponseRedirect("/lab-4")
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)